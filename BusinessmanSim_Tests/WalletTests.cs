using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using BusinessmanSim.Items;

namespace BusinessmanSim_Tests
{
    [TestClass]
    public class WalletTests
    {
        [TestMethod]
        public void CreatingTests()
        {       
            
            Assert.ThrowsException<InvalidOperationException>(() => new Wallet(-1));
            try
            {
                var SomeWallet = new Wallet();
                SomeWallet = new Wallet(200);
                SomeWallet = new Wallet(int.MaxValue);
            }
            catch (Exception exc)
            {
                Assert.Fail(exc.Message);
            }
        }

        [TestMethod]
        public void TakeMethdTests()
        {            
            const int Delta = 20;
            const int InitialAmount = 500;

            var SomeWallet = new Wallet(InitialAmount);

            Assert.IsTrue(SomeWallet.CanTake(InitialAmount - Delta));

            Assert.IsFalse(SomeWallet.CanTake(-Delta));
            Assert.IsFalse(SomeWallet.CanTake(InitialAmount + Delta));

            Assert.ThrowsException<InvalidOperationException>( ()=> SomeWallet.Take(InitialAmount + Delta));

            try
            {
                SomeWallet.Take(SomeWallet.Amount)
                    .Put(InitialAmount);

                SomeWallet.Take(InitialAmount - Delta);
            }
            catch (Exception exc)
            {
                Assert.Fail(exc.Message);
            }

            Assert.IsTrue(SomeWallet.Take(SomeWallet.Amount).Amount == 0);
        }

        [TestMethod]
        public void PutMethdTests()
        {
            const int Delta = 20;
            const int InitialAmount = 500;

            var SomeWallet = new Wallet(InitialAmount);

            Assert.IsTrue(SomeWallet.CanPut(Delta));     
            
            Assert.IsFalse(SomeWallet.CanPut(-Delta));
            Assert.IsFalse(SomeWallet.CanPut(int.MaxValue));

            Assert.ThrowsException<InvalidOperationException>(() => SomeWallet.Put(-Delta));
            Assert.ThrowsException<InvalidOperationException>(() => SomeWallet.Put(int.MaxValue));

            try
            {
                SomeWallet.Take(SomeWallet.Amount)
                    .Put(Delta);
            }
            catch (Exception exc)
            {
                Assert.Fail(exc.Message);
            }

            Assert.IsTrue(SomeWallet.Take(SomeWallet.Amount).Put(Delta).Amount == Delta);
        }
    }
}
