﻿using System;
using BusinessmanSim.IndustrialBuildings.Configuration;
using BusinessmanSim.IndustrialBuildings.ResourceStorages;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BusinessmanSim.IndustrialBuildings.Resources;
using BusinessmanSim.IndustrialBuildings.PowerPlants;
using System.Collections.Generic;

namespace BusinessmanSim_Tests
{
    [TestClass]
    public class NuclearPowerPlantTests
    {
        [TestMethod]
        public void CreatingTests()
        {
            var cfg = new NuclearPowerPlantConfig();
            cfg.InitialReactorCount = 2;
            cfg.MaxReactorCount = 10;
            cfg.ReactorPower = 200;
            cfg.ReactorResCount = 20;

            var StorageSize = 240;

            IStorage SomeStorage = new ResourceStorage(ResourceType.NuclearFuel, StorageSize);
            SomeStorage.PutResource(StorageSize);

            cfg.ReactorResCount = -20;
            Assert.ThrowsException<ArgumentException>(() => { NuclearPowerPlant PowerPlant = new NuclearPowerPlant(cfg); });
            cfg.ReactorResCount = 20;

            NuclearPowerPlant PowerPlant = null;

            try
            {
                PowerPlant = new NuclearPowerPlant(cfg);
                for (int i = 0; i < 10; i++) PowerPlant.AddRactor();
                for (int i = 0; i < 20; i++) PowerPlant.RemoveRactor();
            }
            catch (Exception exc)
            {
                Assert.Fail(exc.Message);
            }

            PowerPlant = new NuclearPowerPlant(cfg);

            PowerPlant.AddResourceStorage(SomeStorage);

            var EnergyGenerateStatistic = new List<double>();

            for (int i = 0; i < 100; i++)
            {
                EnergyGenerateStatistic.Add(PowerPlant.GenerateEnergy());
            }

            Assert.ThrowsException<ArgumentException>(() => { PowerPlant.AddResourceStorage(SomeStorage); });
            Assert.ThrowsException<ArgumentException>(() => { PowerPlant.AddResourceStorage(null); });
            Assert.ThrowsException<ArgumentException>(() => { PowerPlant.AddResourceStorage(new ResourceStorage(ResourceType.Wood, 20)); });

            SomeStorage.PutResource(182);

            var EnergyGenerateStatistic2 = new List<double>();

            for (int i = 0; i < 100; i++)
            {
                EnergyGenerateStatistic2.Add(PowerPlant.GenerateEnergy());
            }


        }
    }
}
