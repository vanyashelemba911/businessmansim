﻿using BusinessmanSim.IndustrialBuildings.Resources;
using System;

namespace BusinessmanSim.IndustrialBuildings.ResourceStorages
{
    public class ResourceStorage : IStorage
    {
        public int MaxSize { get; private set; }
        public int Capacity { get; private set; }
        
        public ResourceType Resource { get; private set; }

        public ResourceStorage(ResourceType resourceType, int maxSize)
        {
            if (maxSize <= 0)
                throw new ArgumentException("`maxSize` must be greater than 0");
            
            MaxSize = maxSize;
            Resource = resourceType;            
        }

        public bool CanGetResource(int count) => count >= 0 && Capacity - count >= 0;
        public bool CanPutResource(int count) => count >= 0 && MaxSize - Capacity >= count;

        public int GetResource(int count)
        {
            if (CanGetResource(count))
            {
                Capacity -= count;
                return count;
            }

            count = Capacity;
            Capacity = 0;

            return count;
        }

        public void PutResource(int count)
        {
            if (!CanPutResource(count))
            {
                throw new ArgumentException($"Can`t put {count} resourceses. (Capacity = {Capacity}, MaxSize={MaxSize})");
            }

            Capacity += count;
        }
    }
}
