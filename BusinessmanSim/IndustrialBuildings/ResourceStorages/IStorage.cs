﻿using BusinessmanSim.IndustrialBuildings.Resources;

namespace BusinessmanSim.IndustrialBuildings.ResourceStorages
{
    public interface IStorage
    {
        public int MaxSize { get; }
        public int Capacity { get; }

        public ResourceType Resource { get; }        

        public int GetResource(int count);
        public void PutResource(int count);

        public bool CanGetResource(int count);
        public bool CanPutResource(int count);
    }
}
