﻿using System;
using BusinessmanSim.IndustrialBuildings.ResourceStorages;
using System.Collections.Generic;
using BusinessmanSim.IndustrialBuildings.Configuration;
using System.Text;

namespace BusinessmanSim.IndustrialBuildings.PowerPlants
{
    public class NuclearPowerPlant : IPowerPlant
    {
        private readonly int maxReactors;
        private readonly int reactorPower;
        private readonly int resourcesForReactor;

        private readonly List<IStorage> resourceStorages;

        public int ReactorsCount { get; private set; }

        public NuclearPowerPlant(NuclearPowerPlantConfig config)
        {
            if (!ConfigIsValid(config))
                throw new ArgumentException(GetFailMessage(config));
                        
            reactorPower = config.ReactorPower;
            maxReactors = config.MaxReactorCount;
            ReactorsCount = config.InitialReactorCount;
            resourcesForReactor = config.ReactorResCount;

            resourceStorages = new List<IStorage>();
        }

        private bool ConfigIsValid(NuclearPowerPlantConfig config)
        {
            return 
                config.MaxReactorCount > 0 && 
                config.InitialReactorCount >= 0 &&
                config.ReactorPower > 0 &&
                config.ReactorResCount > 0;
        }

        private String GetFailMessage(NuclearPowerPlantConfig config)
        {
            StringBuilder Message = new StringBuilder();

            Message.Append("Fail to crate NuclearPowerPlant with arguments:");
            Message.Append($"MaxReactors={config.MaxReactorCount},");
            Message.Append($"ReactorPower={config.ReactorPower},");
            Message.Append($"InitialReactorCount={config.InitialReactorCount};");
            Message.Append($"ReactorResCount={config.ReactorResCount};");

            return Message.ToString();
        }

        public NuclearPowerPlant AddResourceStorage(IStorage someStorage)
        {
            if (someStorage?.Resource != Resources.ResourceType.NuclearFuel)
                throw new ArgumentException("Invalid storage for NuclearPowerPlant");
            
            if (resourceStorages.Contains(someStorage))
                throw new ArgumentException("Storage already exists in storage list");

            resourceStorages.Add(someStorage);

            return this;
        }

        public NuclearPowerPlant AddRactor()
        {
            if(ReactorsCount < maxReactors)
                ReactorsCount++;

            return this;
        }

        public NuclearPowerPlant RemoveRactor()
        {
            if(ReactorsCount > 0)
                ReactorsCount--;

            return this;
        }

        public double GenerateEnergy()
        {
            return GenerateForAvailableResources(getAvailableResources(ReactorsCount * resourcesForReactor));
        }

        private int getAvailableResources(int neededResources)
        {
            int AvailableResources = 0;

            foreach(IStorage SomeStorage in resourceStorages)
            {
                if (SomeStorage.CanGetResource(neededResources))
                {
                    AvailableResources += SomeStorage.GetResource(neededResources);
                    break;
                }
                AvailableResources += SomeStorage.GetResource(neededResources);
            }

            return AvailableResources;
        }

        private double GenerateForAvailableResources(int availableResources)
        {
            double neededResources = ReactorsCount * resourcesForReactor;
            double Koef = availableResources / neededResources;
            double FullPowerCreating = reactorPower * ReactorsCount;    

            return FullPowerCreating * Koef;
        }
    }
}
