﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessmanSim.IndustrialBuildings.PowerPlants
{
    public interface IPowerPlant
    {
        public double GenerateEnergy();
    }
}