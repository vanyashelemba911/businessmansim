﻿using System;

namespace BusinessmanSim.IndustrialBuildings.Configuration
{
    [Serializable]
    public class NuclearPowerPlantConfig
    {
        public int ReactorPower { get; set; }
        public int ReactorResCount { get; set; }        
        public int MaxReactorCount { get; set; }
        public int InitialReactorCount { get; set; }        
    }
}