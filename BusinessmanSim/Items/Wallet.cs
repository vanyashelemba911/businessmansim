﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BusinessmanSim.Items
{
    public class Wallet
    {
        public int Amount { get; private set; }

        public Wallet() : this(0) { }
        public Wallet(int value)
        {
            if (value >= 0)
            {
                Amount = value;
            }
            else
            {
                ThrowInvalidOperationExc($"Attempting to initialize Wallet with value: {value}");
            }
        }

        public bool CanTake(int value) => value >= 0 && Amount - value >= 0;
        public bool CanPut(int value) => value >= 0 && int.MaxValue - Amount > value;

        public Wallet Take(int value)
        {
            if (CanTake(value))
            {
                Amount -= value;
            }
            else
            {
                ThrowInvalidOperationExc($"Attempting to take {value} from wallet with amount - {Amount}");
            }

            return this;
        }

        public Wallet Put(int value)
        {
            if (CanPut(value))
            {
                Amount += value;
            }
            else
            {
                ThrowInvalidOperationExc($"Attempting to put {value} in wallet with amount - {Amount}, this operation may trigger an overflow");
            }

            return this;
        }
        
        private void ThrowInvalidOperationExc(String message)
        {
            throw new InvalidOperationException(message);
        }
    }
}
