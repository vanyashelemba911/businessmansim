﻿using System;

namespace BusinessmanSim.People
{
    class Employee
    {
        private int maxSkillPoints;
        public int SkillPoints { get; private set; }

        public Employee(int initialSkill, int maxSkill)
        {
            if (initialSkill < 0 || maxSkill <= 0) 
                throw new ArgumentException();

            maxSkillPoints = maxSkill;
            SkillPoints = initialSkill;
        }

        public void IncreaseSkill(int value)
        {
            if (value <= 0) return;

            SkillPoints = (SkillPoints + value <= maxSkillPoints) ? SkillPoints + value : maxSkillPoints;
        }

        public void DecreaseSkill(int value)
        {
            if (value <= 0) return;

            SkillPoints = (SkillPoints - value > 0) ? SkillPoints - value : 0;
        }
    }
}